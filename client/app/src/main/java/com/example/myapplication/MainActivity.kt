package com.example.myapplication

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_gallery.*
import java.net.Socket
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_gallery.applyButton
import android.view.View
import android.view.ViewGroup


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

                // var updateButton = findViewById<Button>(R.id.applyButton)
       // if (updateButton != null) {
          //  updateButton.setOnClickListener()
         //   {
           //     Log.e("Openclck.LOG_TAG", "КНОПКА ЧТО-ТО ДЕЛАЕТ??")

          //  }
       //11 }

/*
        var mColorCheckBox: ConstraintLayout = findViewById(R.id.checkBoxLenta)
        var root: RelativeLayout = findViewById(R.id.l_settings)

        applyButton.setOnClickListener {
            if (mColorCheckBox.isSelected)
                Snackbar.make(root, "Регистрация прошла успешно!", Snackbar.LENGTH_LONG).show()
        }*/

        //var mColorCheckBox: ConstraintLayout = findViewById(R.id.applyButton)
        //var root: RelativeLayout = findViewById(R.id.l_settings)
        //mColorCheckBox.setOnClickListener()
       // {
          //  Log.e(Openclck.LOG_TAG, "КНОПКА ЧТО-ТО ДЕЛАЕТ??")

           // Snackbar.make(this, "Регистрация прошла успешно!", Snackbar.LENGTH_LONG).show()

       // }

    }





    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)


        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
       when (item.itemId) {
           R.id.action_settings -> {

               this.onBackPressed()
               return true

           }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun main() {
        val client = Socket("127.0.0.1", 8005)
        client.outputStream.write("Hello from the client!".toByteArray(Charsets.UTF_16LE))
        client.close()
    }













}



