package com.example.myapplication;
import android.content.Context;
//import android.support.v7.widget.PopupMenu;
//import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Display;
import android.widget.PopupMenu;
import android.net.Uri;
import android.content.Intent;


import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<News> listItems;
    private Context mContext;

    public MyAdapter(List<News> listItems, Context mContext) {
        this.listItems = listItems;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.source_news, parent, false);

        return new ViewHolder(v);


    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final News itemList = listItems.get(position);
        holder.txtTitle.setText(itemList.getTitle());
        holder.txtDescription.setText(itemList.getDescription());


        holder.txtOptionDigit.setOnClickListener(v -> {

            PopupMenu popupMenu = new PopupMenu(mContext, holder.txtOptionDigit);
            popupMenu.inflate(R.menu.option_menu);
            popupMenu.setOnMenuItemClickListener(item -> {

                switch (item.getItemId()) {


                    case R.id.menu_item_delete:
                        //Delete item
                        listItems.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(mContext, "Удалено", Toast.LENGTH_LONG).show();
                        break;
                    default:
                        break;
                }
                return false;
            });
            popupMenu.show();
        });

        holder.txtTitle.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(itemList.getURL()));
            mContext.startActivity(browserIntent);
            //Toast.makeText(mContext, "Deleted", Toast.LENGTH_LONG).show();
        });




    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtTitle;
        public TextView txtDescription;
        public TextView txtOptionDigit;
        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.source_news_name);
            txtDescription = (TextView) itemView.findViewById(R.id.txtDescription);
            txtOptionDigit = (TextView) itemView.findViewById(R.id.txtOptionDigit);
        }
    }

   
}