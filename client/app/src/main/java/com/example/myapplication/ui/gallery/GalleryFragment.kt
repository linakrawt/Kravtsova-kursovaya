package com.example.myapplication.ui.gallery

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.Openclck
import com.example.myapplication.R
import kotlinx.android.synthetic.main.fragment_gallery.view.*

class GalleryFragment : Fragment() {

    private lateinit var galleryViewModel: GalleryViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        galleryViewModel =
                ViewModelProvider(this).get(GalleryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)


        root.applyButton.setOnClickListener {
            var URL: String = ""
            if (root.checkBoxLenta.isChecked)
                URL += "http://lenta.ru/rss/news" + "|"
            if (root.checkBoxRia.isChecked)
                URL += "https://ria.ru/export/rss2/archive/index.xml" + "|"
            if (root.checkBoxNews.isChecked)
                URL += "https://www.gazeta.ru/export/rss/first.xml" + "|"
            if (root.checkBoxHabr.isChecked)
                URL += "https://habr.com/ru/rss/hubs/all/" + "|"
            if (root.checkBoxTp.isChecked)
                URL += "https://tproger.ru/rss" + "|"
            if (root.checkBoxIxbt.isChecked)
                URL += "http://www.ixbt.com/export/news.rss" + "|"
            if (root.checkBoxDtf.isChecked)
                URL += "http://dtf.ru/rss/new" + "|"

                    //Log.d("btnSetup", "ЛЕНТА")
                 else Log.d("btnSetup", "Кнопка")


                Openclck().OnsendData("$URL ")
                Thread.sleep(1000)

            }



        return root
    }
}