package com.example.myapplication


import android.content.DialogInterface
import android.content.DialogInterface.OnClickListener
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AlertDialog.Builder
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.Openclck.value
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import java.security.MessageDigest




     fun hash( message: String): String{
        val bytes = message.toByteArray()
        val md = MessageDigest.getInstance("SHA-256")
        val digest = md.digest(bytes)
        return digest.fold("", { str, it -> str + "%02x".format(it) })
    }


public class login : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginButton.setOnClickListener {
            var root: RelativeLayout = findViewById(R.id.root_element)
            var login = editTextLogin.text.toString()
            var passwd = editTextPassword.text.toString()

            if (login.toString().isEmpty() || passwd.toString().isEmpty()) {
                Snackbar.make(root, "Введены не все данные!", Snackbar.LENGTH_LONG).show()


            } else {

                Openclck().OnsendData(login, hash(passwd), 0)
                Thread.sleep(1000)
                if (value.toInt() > 0) {

                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)


                } else Snackbar.make(root, "Ошибка авторизации", Snackbar.LENGTH_LONG).show()

            }
        }
            button2.setOnClickListener {

                ShowRegisterWindow();
            }
    }

    fun ShowRegisterWindow() {
        var root: RelativeLayout = findViewById(R.id.root_element)
        val dialog: AlertDialog.Builder = Builder(this)
        dialog.setTitle("Зарегистрироваться")
        val inf = LayoutInflater.from(this)
        val register_window: View = inf.inflate(R.layout.register_window, null)
        dialog.setView(register_window)
        val login: EditText = register_window.findViewById(R.id.Email)
        val passwd: EditText = register_window.findViewById(R.id.Password)


        dialog.setNegativeButton("Отмена") { dialogInterface, which ->
            dialogInterface.dismiss()
        }

        dialog.setPositiveButton("Добавить", object : OnClickListener {
            override fun onClick(dialogInterface: DialogInterface?, wich: Int) {
                if (login.text.toString().isEmpty() || passwd.toString().isEmpty()) {
                    Snackbar.make(root, "Введены не все данные!", Snackbar.LENGTH_LONG).show()

                    return
                }
                if (passwd.text.toString().length < 8) {
                    Snackbar.make(root, "Пароль меньше 8 символов!", Snackbar.LENGTH_LONG).show()
                    return
                } else {

                    Openclck().OnsendData(login.getText().toString(), hash(passwd.getText().toString()), 1)
                    Thread.sleep(1000)
                    if (value.toInt() > 0)
                        Snackbar.make(root, "Регистрация прошла успешно!", Snackbar.LENGTH_LONG).show()
                    else Snackbar.make(root, "Пользователь уже существует!!", Snackbar.LENGTH_LONG).show()

                }
            }
        })

        dialog.show()

    }
}


