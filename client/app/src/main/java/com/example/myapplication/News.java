package com.example.myapplication;

public class News {


    private String title;
    private String description;
    private String URL;
    private String PictureURL;

    public News(String title, String description, String url) {
        this.title = title;
        this.description = description;
        this.URL = url;
       // this.PictureURL = urlPic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String url) {
        this.URL = url;
    }

    /*public String getPictureURL() {
        return PictureURL;
    }

    public void setPictureURL(String urlPic) {
        this.PictureURL = urlPic;
    }*/


}

