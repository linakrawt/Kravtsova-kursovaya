﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;


namespace Server
{
    public class BD
    {

   

        SQLiteCommand command = Program.DB.CreateCommand();
        public bool Check(string Data)
        {
            bool check;
            string log = "", passwd = "";
            int K = 0;
            for (int i = 3; i < Data.Length - 1; i++)
            {
                if (Data[i] == '|')
                {
                    K = 1;
                }
                if (K == 0)
                    log += Data[i];
                else passwd += Data[i + 1];

            }
            // bool check = new User(log, passw).CheckUser();
            command.Parameters.Add("@log", DbType.String).Value = log;
            command.Parameters.Add("@passwd", DbType.String).Value = passwd;
            command.CommandText = "select * from User where Login like @log and Password like @passwd";
            if ((command.ExecuteScalar() != null))
                check = true;
            else check = false;

            return check;
        }

        public bool Reg(string Data)
        {
            SQLiteCommand command = Program.DB.CreateCommand();
            bool check;
            string log = "", passwd = "";
            int K = 0;
            for (int i = 3; i < Data.Length - 1; i++)
            {
                if (Data[i] == '|')
                {
                    K = 1;
                }
                if (K == 0)
                    log += Data[i];
                else passwd += Data[i + 1];

            }
            command.Parameters.Add("@log", DbType.String).Value = log;
            command.Parameters.Add("@passwd", DbType.String).Value = passwd;
            command.CommandText = "select * from User where Login like @log";
            if (command.ExecuteScalar() == null)
            {
                command.CommandText = "insert into User(Login, Password) values(@log, @passwd)";
                command.ExecuteNonQuery();
                check = true;
            }
            else check = false;
            return check;
        }
    }
}