﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.Syndication;
using System.Net.Http;
using Microsoft.Toolkit.Parsers.Rss;
using System.Xml;
using System.Xml.Serialization;
using System.IO;



namespace Microsoft.Toolkit
{

    public class RSSparser
    {
        public static string feed = null;
        static string title = "";


        public static string ParseRSS(string builder)
        {
            title = "";
            feed = null;

            using (var client = new HttpClient())
            {
                try
                {
                    var Feed = client.GetStringAsync(builder);
                    Feed.Wait();
                    Feed.ContinueWith((t) =>
                    {
                        var tresult = t.Result;
                        feed = tresult;
                    });
   
                }
                catch { }
            }

            if (feed != null)
            {
                var parser = new RssParser();
                var rss = parser.Parse(feed);
                //var element ;
               // for (int i =0; i<10; ++i)
                foreach (var element in rss)
                {
                    title += $"{element.Title}|"  + $"{element.Summary}|"   + $"{element.FeedUrl}|";
                }
                // Console.WriteLine(title);
            }
            return title;
        }

    }
}